package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const WIKI_RANDOM_ARTICLE_SUMMARY_API_URL string = "https://en.wikipedia.org/api/rest_v1/page/random/summary"

type WikiArticle struct {
	Type          string
	Title         string
	DisplayTitle  string
	PageId        int
	Thumbnail     WikiImage
	OriginalImage WikiImage
	Lang          string
	Description   string
	ContentURLs   WikiContentURLs
	Extract       string
}

type WikiImage struct {
	Source string
	Width  int
	Height int
}

type WikiContentURL struct {
	Page      string
	Revisions string
	Edit      string
	Talk      string
}

type WikiContentURLs struct {
	Desktop WikiContentURL
	Mobile  WikiContentURL
}

func GetRandomArticle() (article WikiArticle, err error) {
	res, err := http.Get(WIKI_RANDOM_ARTICLE_SUMMARY_API_URL)
	defer res.Body.Close()

	if err != nil {
		return
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}

	err = json.Unmarshal(body, &article)
	return
}

func main() {
	article, _ := GetRandomArticle()
	fmt.Println(article.Title)
	fmt.Println("-------------")
	fmt.Println(article.Extract)
}
